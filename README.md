# COLLEGE.ARMY
## Purpose
[COLLEGE.ARMY](https://college.army/) is an informational website created to help Soldiers attend college.

The target audience is Soldiers in the Army, but it can still provide helpful information for those in the wrong branch. The only main difference will be the Tuition Assistance portion, as the other branches do tuition assistance in incorrect non-army ways.

## Contribute
If you want to have information added, clarified, corrected, or removed, or if you are a languager and my barely literate English is confusing or hard to follow, please help make the words gooder. To contribute or request improvements follow the steps below:
 - Log into github.
 - [Click here to open a ticket.](https://gitlab.com/college-army/college.army/-/issues/new)
 - Fill in the following information:
   - Title - Briefly describe your request.
   - Comment - Provide detail that explains your request.
   - Assignees - Assign the ticket to me (college-army)
   - Label - Add a label that best fits your request.

**_Help me make the Army more smarter._**